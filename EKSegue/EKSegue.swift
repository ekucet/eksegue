//
//  EKSegue.swift
//  EKSegue
//
//  Created by Erkam Kucet on 12/08/15.
//  Copyright (c) 2015 erkam kucet. All rights reserved.
//

import UIKit

class EKSegue: UIStoryboardSegue {
    
    override func perform() {
        
        let sourceVC: UIViewController = self.sourceViewController as! UIViewController
        let destinationVC: UIViewController = self.destinationViewController as! UIViewController
        
        sourceVC.view.addSubview(destinationVC.view)
        
        destinationVC.view.transform = CGAffineTransformMakeScale(0.05, 0.05)
        
        UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
            
            destinationVC.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
            
        }) { (status) -> Void in
            
            destinationVC.view.removeFromSuperview()
            
            let time = dispatch_time(DISPATCH_TIME_NOW, Int64(0.001 * Double(NSEC_PER_SEC)))
            
            dispatch_after(time, dispatch_get_main_queue(), { () -> Void in
                
                sourceVC.presentViewController(destinationVC, animated: false, completion: nil)
            })
        }
        
    }
    
}
